/**
 * 冒泡排序函数
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int[] a, int n) {
    if (n <= 1) {
        return;
    }

    /* 最后一次交换的位置 */
    int lastExchangeIndex = 0;
    /* 无序数列的边界，每次只需要比较到这里即可退出循环 */
    int sortedBorder = n - 1;

    for (int i = 0; i < n; i++) {
        /* 提前退出冒泡循环的标志位 */
        boolean swapped = false;

        for (int j = 0; j < sortedBorder; j++) {
            if (a[j] > a[j + 1]) {
                /* 交换元素 */
                int tmp = a[j];
                a[j] = a[j + 1];
                a[j + 1] = tmp;

                /* 更新最后一次交换的位置 */
                lastExchangeIndex = j;
                swapped = true;
            }
        }

        /* 更新无序数列的边界 */
        sortedBorder = lastExchangeIndex;

        /* 如果一次循环中没有发生元素交换，说明已经排序完成，提前退出 */
        if (!swapped) {
            break;
        }
    }
}

package codes.tianya66;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        String[] split = "2,4,1,2,1,10,9,8".split(",");
        int[] array = Arrays.stream(split).mapToInt(Integer::valueOf).toArray();
        System.out.println(Arrays.toString(sort(array)));
    }

    public static int[] sort(int[] array) {
        int length = array.length;
        if (length <= 1) {
            return array;
        }
        for (int i = 0; i < length - 1; i++) {
            for (int j = 0; j < length - 1 - i; j++) {
                if (array[j] > array[j + 1]) {
                    int temp = array[j + 1];
                    array[j + 1] = array[j];
                    array[j] = temp;
                }
            }
        }
        return array;
    }
}

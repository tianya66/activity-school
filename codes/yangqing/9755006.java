/**
 * 冒泡排序函数
 * aa bb cc
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int [] a, int n){
    // 你的代码，使无序数组 a 变得有序
    for(int i=0;i<a.length-1;i++){
        for(int j=0;j<a.length-1-i;j++){
            if(a[j]>a[j+1]){
                n=a[j];
                a[j]=a[j+1];
                a[j+1]=n;
            }
        }
    }
    for(int i=0;i<a.length;i++)
        System.out.println(a[i]);
} //end